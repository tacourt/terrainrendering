#version 330

// input attributes 
layout(location = 0) in vec3 position; 
layout(location = 2) in vec2 texCoordsBuffer; 

// input uniforms
uniform mat4 mdvMat;      // modelview matrix 
uniform mat4 projMat;     // projection matrix
uniform mat3 normalMat;   // normal matrix
uniform vec3 light;
uniform vec3 motion;
uniform vec3 timer;
uniform float time;
uniform sampler2D lavaHeight;
uniform sampler2D stoneDisplacement;

// out variables 
out vec3 normalView;
out vec3 tangentView;
out vec3 binormalView;
out vec3 eyeView;
out float height;
out mat3 TBN;
out vec2 stoneTexCoords;
out vec2 lavaTexCoords;
out float depth;

// fonctions utiles pour créer des terrains en général
vec2 hash(vec2 p) {
  p = vec2( dot(p,vec2(127.1,311.7)),
	    dot(p,vec2(269.5,183.3)) );  
  return -1.0 + 2.0*fract(sin(p)*43758.5453123);
}

float gnoise(in vec2 p) {
  vec2 i = floor(p);
  vec2 f = fract(p);
	
  vec2 u = f*f*(3.0-2.0*f);
  
  return mix(mix(dot(hash(i+vec2(0.0,0.0)),f-vec2(0.0,0.0)), 
		 dot(hash(i+vec2(1.0,0.0)),f-vec2(1.0,0.0)),u.x),
	     mix(dot(hash(i+vec2(0.0,1.0)),f-vec2(0.0,1.0)), 
		 dot(hash(i+vec2(1.0,1.0)),f-vec2(1.0,1.0)),u.x),u.y);
}

float pnoise(in vec2 p,in float amplitude,in float frequency,in float persistence, in int nboctaves) {
  float a = amplitude;
  float f = frequency;
  float n = 0.0;
  
  for(int i=0;i<nboctaves;++i) {
    n = n+a*gnoise(p*f);
    f = f*2.;
    a = a*persistence;
  }
  
  return n;
}


float computeHeight(in vec2 p) {

  // Bruit de perlin
  return pnoise(p+motion.xy,0.5,1.5,0.5,3);
  
  // version plan
  return 0.0;
  
  // version sinus statique
  //return 0.5*sin(p.x*10);

  // version sinus animé 
  // return 0.2*sin((p.x+motion.x*10)*30);
}

vec3 computeTangent(vec3 n) {
	vec3 v = vec3(1.0, 0.0, 0.0);
	float d = dot(v, n);
	if (abs(d) < 1.0e-3) {
		v = vec3(0.0, 1.0, 0.0);
		d = dot(v, n);
	}
	return normalize(v - d * n);
}


vec3 computeNormal(in vec2 p) {
  const float EPS = 0.01;
  const float SCALE = 2000.;
  
  vec2 g = vec2(computeHeight(p+vec2(EPS,0.))-computeHeight(p-vec2(EPS,0.)),
		computeHeight(p+vec2(0.,EPS))-computeHeight(p-vec2(0.,EPS)))/2.*EPS;
  
  vec3 n1 = vec3(1.,0.,g.x*SCALE);
  vec3 n2 = vec3(0.,1.,-g.y*SCALE);
  vec3 n = normalize(cross(n1,n2));

  return n;
}

vec2 computeTexCoords(vec2 position){
  return (position / 2 + 0.5f) + motion.xy;
}

void main() {
  stoneTexCoords = computeTexCoords( position.rg) + time/2;
  lavaTexCoords = stoneTexCoords + time;
  height = computeHeight(stoneTexCoords);

  float a = smoothstep(-0.20,0.05f , height);
  float lavaHeight = height - texture(lavaHeight,lavaTexCoords).r*0.6f;
  float stoneHeight = height + texture(stoneDisplacement,stoneTexCoords).r*0.1f;
  height = mix(lavaHeight,stoneHeight,a);


  // if(height > 0.f){
  //   height = height + texture(stoneDisplacement,texCoords).r*0.2f ;
  // }else{
  //   height = height - texture(lavaHeight,texCoords).r*0.2f ;
  // }


  vec3 p = vec3(position.xy,height);
  gl_Position =  projMat*mdvMat*vec4(p,1);
  vec3  normal = computeNormal(stoneTexCoords);
  vec3 tangent = computeTangent(normal);
  
  normalView  = normalize(normalMat*normal);
  eyeView     = normalize((mdvMat*vec4(p,1.0)).xyz);
  tangentView = normalize(normalMat*tangent);
  binormalView = cross(normalView, tangentView);
  mat3 TBN = mat3(tangentView,binormalView,normalView);
  // c'est la profondeur depuis le point de vue qui est calculée ici
  depth       = -(mdvMat*vec4(position,1.0)).z/30.0;
}
