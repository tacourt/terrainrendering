#version 330
#define PI 3.1415926535897932384626433832795


// input uniforms 
uniform vec3 light;
uniform vec3 motion;

// in variables 
in vec3  normalView;
in vec3  eyeView;
in vec2  stoneTexCoords;
in vec2  lavaTexCoords;
in float height;
in float time;
in float depth;
uniform sampler2D lavaColor;
uniform sampler2D lavaEmissive;
uniform sampler2D lavaNormals;
uniform sampler2D lavaAO;
uniform sampler2D lavaAlbedo;
uniform sampler2D lavaRoughness;

uniform sampler2D stoneColor;
uniform sampler2D stoneAO;
uniform sampler2D stoneNormals;
uniform sampler2D stoneRoughness;

// the 2 output locations: refers to glDrawBuffers in the cpp file 
layout(location = 0) out vec4 outBuffer1;
layout(location = 1) out vec4 outBuffer2;

// took here : https://gist.github.com/ishikawash/3287209
vec2 to2D(vec3 R){
  float u = ( atan(R.x,R.z)+ PI ) / 2*PI;
  float v = acos(-R.y) / PI;
  return vec2(u,v);
}

void main() {
  vec3 ambient  = vec3(0.3,0.3,0.2);
  vec3 diffuse  = vec3(0.3,0.5,0.8);
  vec3 specular = vec3(1.0,0.5,0.);
  float et = 50.0;

  vec3 n = normalize(normalView);
  if( height < 0.f){ // pour la lave
    n = normalize(texture(lavaNormals,lavaTexCoords).rgb);
    ambient  = texture(lavaAO,lavaTexCoords).rgb;
    diffuse  = texture(lavaRoughness,lavaTexCoords).rgb;
    specular = texture(lavaEmissive,lavaTexCoords).rgb;
    et = 50.0;
  }else{ // pour la pierre
    n = normalize(texture(stoneNormals,stoneTexCoords).rgb);
    ambient  = texture(stoneAO,stoneTexCoords).rgb - 0.5f;
    diffuse  = texture(stoneRoughness,stoneTexCoords).rgb;
    specular = vec3(1.0,0.5,0.);
  }
  
  vec3 e = normalize(eyeView);
  vec3 l = normalize(light);

  float diff = dot(l,n);
  float spec = pow(max(dot(reflect(l,n),e),0.0),et);

  vec3 phong = ambient + diff*diffuse + specular*spec;


  vec4 stoneTexColor = vec4(texture(stoneColor,stoneTexCoords+motion.xy).rgb, 1.0);
  vec4 lavaTexColor = vec4(texture(lavaColor,lavaTexCoords+motion.xy).rgb, 1.0);
  vec4 emissiveColor = texture(lavaEmissive,lavaTexCoords);


  float a = smoothstep(-0.05,0.05f , height);
  vec4 color = mix(lavaTexColor*emissiveColor,stoneTexColor,a);

  // float b = smoothstep(-0.05f,0.f,height);
  // color = mix(color*3.f, color,b);

  float c = smoothstep(-0.40f,-0.10f,height);
  color = mix(lavaTexColor*4.f, color,c);
  // color = mix(color*newC, color,c);

  vec4 outColor = vec4(phong,1.0f) * color;
  // outColor = vec4(texCoords,0.,1.);

  outBuffer1 = outColor;

  // on utilise ici la 2ieme texture pour stocker la profondeur
  // comme le canal alpha n'était pas utilisé, on l'utilise pour la profondeur. 
  outBuffer2 = vec4(n,depth);
}
