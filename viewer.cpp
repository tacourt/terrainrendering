#include "viewer.h"

#include <math.h>
#include <iostream>
#include <QTime>

using namespace std;

Viewer::Viewer(char *,const QGLFormat &format)
  : QGLWidget(format),
    _timer(new QTimer(this)),
    _light(glm::vec3(0,0,1)),
    _motion(glm::vec3(0,0,0)),
    _mode(false),
    _currentshader(0),
    _time(0),
    _ndResol(2048) {

  setlocale(LC_ALL,"C");

  _grid = new Grid(_ndResol,-3.00f,3.0f);
  _cam  = new Camera(1.0f,glm::vec3(0.0f,0.0f,0.0f));

  _timer->setInterval(10);
  connect(_timer,SIGNAL(timeout()),this,SLOT(updateGL()));
}

Viewer::~Viewer() {
  delete _timer;
  delete _grid;
  delete _cam;

  // delete all GPU objects
  deleteShaders();
  deleteVAO();
  deleteTextures();
}

void Viewer::sendTexture(GLuint texID, QImage texture){
  // activate this texture (the current one)
  glBindTexture(GL_TEXTURE_2D,texID);

  // set texture parameters 
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

  // transfer data from CPU to GPU memory
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA32F,texture.width(),texture.height(),0,
  	       GL_RGBA,GL_UNSIGNED_BYTE,(const GLvoid *)texture.bits());

  // generate mipmaps 
  glGenerateMipmap(GL_TEXTURE_2D);

}

void Viewer::createTextures() {
  QImage lavaColor, lavaEmissive, lavaNormals, lavaAO, lavaAlbedo, lavaHeight, lavaRoughness;
  QImage stoneAO, stoneColor, stoneDisplacement, stoneNormals, stoneRoughness;
  QImage distortion;

  
  // enable the use of 2D textures 
  glEnable(GL_TEXTURE_2D);
  // create one texture on the GPU
  glGenTextures(7,_lavaTexIds);
  glGenTextures(5,_stoneTexIds);
  glGenTextures(1,&_distortionMapId);

  // load an image (CPU side)
  
  lavaColor = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/lava.jpeg"));
  lavaEmissive = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/emissive.jpg"));
  lavaNormals = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/normals.jpg"));
  lavaAO = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/ao.jpg"));
  lavaAlbedo = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/albedo.jpg"));
  lavaHeight = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/height.jpg"));
  lavaRoughness = QGLWidget::convertToGLFormat(QImage("textures/lavaTex/roughness.jpg"));

  stoneColor = QGLWidget::convertToGLFormat(QImage("textures/stoneTex/color.jpg"));
  stoneAO = QGLWidget::convertToGLFormat(QImage("textures/stoneTex/ao.jpg"));
  stoneDisplacement = QGLWidget::convertToGLFormat(QImage("textures/stoneTex/displacement.jpg"));
  stoneNormals = QGLWidget::convertToGLFormat(QImage("textures/stoneTex/normal.jpg"));
  stoneRoughness = QGLWidget::convertToGLFormat(QImage("textures/stoneTex/roughness.jpg"));

  distortion = QGLWidget::convertToGLFormat(QImage("textures/distortionMap.jpg"));

  sendTexture(_lavaTexIds[0],lavaColor);
  sendTexture(_lavaTexIds[1],lavaEmissive);
  sendTexture(_lavaTexIds[2],lavaNormals);
  sendTexture(_lavaTexIds[3],lavaAO);
  sendTexture(_lavaTexIds[4],lavaAlbedo);
  sendTexture(_lavaTexIds[5],lavaHeight);
  sendTexture(_lavaTexIds[6],lavaRoughness);

  sendTexture(_stoneTexIds[0],stoneColor);
  sendTexture(_stoneTexIds[1],stoneAO);
  sendTexture(_stoneTexIds[2],stoneDisplacement);
  sendTexture(_stoneTexIds[3],stoneNormals);
  sendTexture(_stoneTexIds[4],stoneRoughness);

  sendTexture(_distortionMapId,distortion);

}

void Viewer::deleteTextures() {
  glDeleteTextures(7,_lavaTexIds);
  glDeleteTextures(5,_stoneTexIds);
  glDeleteTextures(1,&_distortionMapId);
}

void Viewer::createVAO() {
  // cree les buffers associés au terrain 

  glGenBuffers(2,_terrain);
  glGenVertexArrays(1,&_vaoTerrain);

  // create the VBO associated with the grid (the terrain)
  glBindVertexArray(_vaoTerrain);
  glBindBuffer(GL_ARRAY_BUFFER,_terrain[0]); // vertices 
  glBufferData(GL_ARRAY_BUFFER,_grid->nbVertices()*3*sizeof(float),_grid->vertices(),GL_STATIC_DRAW);
  glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void *)0);
  glEnableVertexAttribArray(0);  
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_terrain[1]); // indices 
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,_grid->nbFaces()*3*sizeof(int),_grid->faces(),GL_STATIC_DRAW); 


  // create the VAO associated with a simple quad
  // 2 triangles that cover the viewport (a bit like in TP1)
  static const GLfloat quadData[] = { 
    -1.0f, -1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
     1.0f,  1.0f, 0.0f,
  };
  
  // create VAO
  glGenVertexArrays(1,&_vaoQuad);
  glGenBuffers(1,&_quad);

  // bind VAO 
  glBindVertexArray(_vaoQuad);

  // send and enable vertices
  glBindBuffer(GL_ARRAY_BUFFER,_quad);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quadData),quadData,GL_STATIC_DRAW);
  glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void *)0);
  glEnableVertexAttribArray(0);

  // back to normal
  glBindVertexArray(0);

}

void Viewer::deleteVAO() {
  glDeleteBuffers(2,_terrain);
  glDeleteVertexArrays(1,&_vaoTerrain);
  glDeleteVertexArrays(1,&_vaoQuad);
  glDeleteBuffers(1,&_quad);
}

void Viewer::createFBO() {
  // Ids needed for the FBO and associated textures 
  glGenFramebuffers(1,&_fbo);
  glGenTextures(1,&_rendNormalId);
  glGenTextures(1,&_rendColorId);
  glGenTextures(1,&_rendDepthId);
}

void Viewer::initFBO() {

 // create the texture for rendering colors
  glBindTexture(GL_TEXTURE_2D,_rendColorId);
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA32F,width(),height(),0,GL_RGBA,GL_FLOAT,NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  // create the texture for rendering normals 
  glBindTexture(GL_TEXTURE_2D,_rendNormalId);
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA32F,width(),height(),0,GL_RGBA,GL_FLOAT,NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  // create the texture for rendering depth values
  glBindTexture(GL_TEXTURE_2D,_rendDepthId);
  glTexImage2D(GL_TEXTURE_2D,0,GL_DEPTH_COMPONENT24,width(),height(),0,GL_DEPTH_COMPONENT,GL_FLOAT,NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  // attach textures to framebuffer object 
  glBindFramebuffer(GL_FRAMEBUFFER,_fbo);
  glBindTexture(GL_TEXTURE_2D,_rendColorId);
  glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,_rendColorId,0);
  glBindTexture(GL_TEXTURE_2D,_rendNormalId);
  glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT1,GL_TEXTURE_2D,_rendNormalId,0);
  glBindTexture(GL_TEXTURE_2D,_rendDepthId);
  glFramebufferTexture2D(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_TEXTURE_2D,_rendDepthId,0);
  glBindFramebuffer(GL_FRAMEBUFFER,0);
}

void Viewer::deleteFBO() {
  // delete all FBO Ids
  glDeleteFramebuffers(1,&_fbo);
  glDeleteTextures(1,&_rendNormalId);
  glDeleteTextures(1,&_rendColorId);
  glDeleteTextures(1,&_rendDepthId);
}

void Viewer::drawQuad() {
  // shader id
  const int id = _shaderSecondPass->id();

  // send shader parameters 
  glUniform3fv(glGetUniformLocation(id,"light"),1,&(_light[0]));

  // send textures
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D,_rendColorId);
  glUniform1i(glGetUniformLocation(id,"colormap"),0);

  glActiveTexture(GL_TEXTURE0+1);
  glBindTexture(GL_TEXTURE_2D,_rendNormalId);
  glUniform1i(glGetUniformLocation(id,"normalmap"),1);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D,_distortionMapId);
  glUniform1i(glGetUniformLocation(id,"distortion"),2);

  glUniform1f(glGetUniformLocation(id,"time"),_time);

  // Draw the 2 triangles !
  glBindVertexArray(_vaoQuad);
  glDrawArrays(GL_TRIANGLES,0,6);
  glBindVertexArray(0);
}

void Viewer::createShaders() {
  _vertexFilenames.push_back("shaders/terrain.vert");
  _fragmentFilenames.push_back("shaders/terrain.frag");


  _shaderSecondPass = new Shader();
  _shaderSecondPass->load("shaders/second-pass.vert","shaders/second-pass.frag");
}


void Viewer::enableShader(unsigned int shader) {
  // current shader ID 
  GLuint id = _shaders[shader]->id();

  // activate the current shader 
  glUseProgram(id);

  // send uniform variables 
  glUniformMatrix4fv(glGetUniformLocation(id,"mdvMat"),1,GL_FALSE,&(_cam->mdvMatrix()[0][0]));
  glUniformMatrix4fv(glGetUniformLocation(id,"projMat"),1,GL_FALSE,&(_cam->projMatrix()[0][0]));
  glUniformMatrix3fv(glGetUniformLocation(id,"normalMat"),1,GL_FALSE,&(_cam->normalMatrix()[0][0]));
  glUniform3fv(glGetUniformLocation(id,"light"),1,&(_light[0]));
  glUniform3fv(glGetUniformLocation(id,"motion"),1,&(_motion[0]));
  glUniform1f(glGetUniformLocation(id,"time"),_time);
  
  // send textures
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[0]);
  glUniform1i(glGetUniformLocation(id,"lavaColor"),0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[1]);
  glUniform1i(glGetUniformLocation(id,"lavaEmissive"),1);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[2]);
  glUniform1i(glGetUniformLocation(id,"lavaNormals"),2);
  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[3]);
  glUniform1i(glGetUniformLocation(id,"lavaAO"),3);
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[4]);
  glUniform1i(glGetUniformLocation(id,"lavaAlbedo"),4);
  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[5]);
  glUniform1i(glGetUniformLocation(id,"lavaHeight"),5);
  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D,_lavaTexIds[6]);
  glUniform1i(glGetUniformLocation(id,"lavaRoughness"),6);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_2D,_stoneTexIds[0]);
  glUniform1i(glGetUniformLocation(id,"stoneColor"),7);
  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_2D,_stoneTexIds[1]);
  glUniform1i(glGetUniformLocation(id,"stoneAO"),8);
  glActiveTexture(GL_TEXTURE9);
  glBindTexture(GL_TEXTURE_2D,_stoneTexIds[2]);
  glUniform1i(glGetUniformLocation(id,"stoneDisplacement"),9);
  glActiveTexture(GL_TEXTURE10);
  glBindTexture(GL_TEXTURE_2D,_stoneTexIds[3]);
  glUniform1i(glGetUniformLocation(id,"stoneNormals"),10);
  glActiveTexture(GL_TEXTURE11);
  glBindTexture(GL_TEXTURE_2D,_stoneTexIds[4]);
  glUniform1i(glGetUniformLocation(id,"stoneRoughness"),11);


  

}

void Viewer::deleteShaders() {

  for(unsigned int i=0;i<_vertexFilenames.size();++i) {
    delete _shaders[i];
  }

  delete _shaderSecondPass;
}

void Viewer::disableShader() {
  // desactivate all shaders 
  glUseProgram(0);
}


void Viewer::reloadShaders() {
  for(unsigned int i=0;i<_vertexFilenames.size();++i) {
    _shaders[i]->reload(_vertexFilenames[i].c_str(),_fragmentFilenames[i].c_str());
  }
}


void Viewer::drawVao() {
  // draw faces 
  glBindVertexArray(_vaoTerrain);
  glDrawElements(GL_TRIANGLES,3*_grid->nbFaces(),GL_UNSIGNED_INT,(void *)0);
  glBindVertexArray(0);
}

void Viewer::paintGL() {
  _time = _time + 0.0005f;
  // activate the created framebuffer object
  glBindFramebuffer(GL_FRAMEBUFFER,_fbo);

  GLenum bufferlist [] = {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1};

  glDrawBuffers(2,bufferlist);

  // allow opengl depth test 
  glEnable(GL_DEPTH_TEST);
  
  // screen viewport
  glViewport(0,0,width(),height());

  // clear buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // tell the GPU to use this specified shader and send custom variables (matrices and others)
  enableShader(_currentshader);

  // actually draw the scene
  drawVao();

  // desactivate fbo
  glBindFramebuffer(GL_FRAMEBUFFER,0);

  // clear everything
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // activate the shader 
  glUseProgram(_shaderSecondPass->id());

  // Draw the triangles !
  drawQuad();

  // disable depth test 
  glDisable(GL_DEPTH_TEST);

  // tell the GPU to stop using this shader 
  disableShader();


}

void Viewer::resizeGL(int width,int height) {
  _cam->initialize(width,height,false);
  glViewport(0,0,width,height);

  // re-init the FBO (textures need to be resized to the new viewport size)
  initFBO();
  updateGL();
}

void Viewer::mousePressEvent(QMouseEvent *me) {
  const glm::vec2 p((float)me->x(),(float)(height()-me->y()));

  if(me->button()==Qt::LeftButton) {
    _cam->initRotation(p);
    _mode = false;
  } else if(me->button()==Qt::MidButton) {
    _cam->initMoveZ(p);
    _mode = false;
  } else if(me->button()==Qt::RightButton) {
    _light[0] = (p[0]-(float)(width()/2))/((float)(width()/2));
    _light[1] = (p[1]-(float)(height()/2))/((float)(height()/2));
    _light[2] = 1.0f-std::max(fabs(_light[0]),fabs(_light[1]));
    _light = glm::normalize(_light);
    _mode = true;
  } 

  updateGL();
}

void Viewer::mouseMoveEvent(QMouseEvent *me) {
  const glm::vec2 p((float)me->x(),(float)(height()-me->y()));
 
  if(_mode) {
    // light mode
    _light[0] = (p[0]-(float)(width()/2))/((float)(width()/2));
    _light[1] = (p[1]-(float)(height()/2))/((float)(height()/2));
    _light[2] = 1.0f-std::max(fabs(_light[0]),fabs(_light[1]));
    _light = glm::normalize(_light);
  } else {
    // camera mode
    _cam->move(p);
  }

  updateGL();
}

void Viewer::keyPressEvent(QKeyEvent *ke) {
  const float step = 0.05;
  if(ke->key()==Qt::Key_Z) {
    glm::vec2 v = glm::vec2(glm::transpose(_cam->normalMatrix())*glm::vec3(0,0,-1))*step;
    if(v[0]!=0.0 && v[1]!=0.0) v = glm::normalize(v)*step;
    else v = glm::vec2(0,1)*step;
    _motion[0] += v[0];
    _motion[1] += v[1];
  }

  if(ke->key()==Qt::Key_S) {
    glm::vec2 v = glm::vec2(glm::transpose(_cam->normalMatrix())*glm::vec3(0,0,-1))*step;
    if(v[0]!=0.0 && v[1]!=0.0) v = glm::normalize(v)*step;
    else v = glm::vec2(0,1)*step;
    _motion[0] -= v[0];
    _motion[1] -= v[1];
  }

  if(ke->key()==Qt::Key_Q) {
    _motion[2] += step;
  }

  if(ke->key()==Qt::Key_D) {
    _motion[2] -= step;
  }

  if(ke->key()==Qt::Key_R) {
    reloadShaders();
  }



  // key a: play/stop animation
  if(ke->key()==Qt::Key_A) {
    if(_timer->isActive())
      _timer->stop();
    else 
      _timer->start();
  }

  // key i: init camera
  if(ke->key()==Qt::Key_I) {
    _cam->initialize(width(),height(),true);
  }
  
  // // key f: compute FPS
  // if(ke->key()==Qt::Key_F) {
  //   int elapsed;
  //   QTime timer;
  //   timer.start();
  //   unsigned int nb = 500;
  //   for(unsigned int i=0;i<nb;++i) {
  //     paintGL();
  //   }
  //   elapsed = timer.elapsed();
  //   double t = (double)nb/((double)elapsed);
  //   cout << "FPS : " << t*1000.0 << endl;
  // }

  // key r: reload shaders 
  if(ke->key()==Qt::Key_R) {
    reloadShaders();
  }

  // space: next shader
  if(ke->key()==Qt::Key_Space) {
    _currentshader = (_currentshader+1)%_shaders.size();
  }

  updateGL();
}

void Viewer::initializeGL() {
  // make this window the current one
  makeCurrent();

  // init and chack glew
  if(glewInit()!=GLEW_OK) {
    cerr << "Warning: glewInit failed!" << endl;
  }

  // init OpenGL settings
  glClearColor(0.0,0.0,0.0,1.0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  glViewport(0,0,width(),height());

  // initialize camera
  _cam->initialize(width(),height(),true);

  // init shaders 
  createShaders();

  // init and load all shader files
  for(unsigned int i=0;i<_vertexFilenames.size();++i) {
    _shaders.push_back(new Shader());
    _shaders[i]->load(_vertexFilenames[i].c_str(),_fragmentFilenames[i].c_str());
  }

  // init VAO/VBO
  createVAO();
  createTextures();
  // create/init FBO
  createFBO();
  initFBO();

  // starts the timer 
  _timer->start();
}

