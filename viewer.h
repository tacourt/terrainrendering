#ifndef VIEWER_H
#define VIEWER_H

// GLEW lib: needs to be included first!!
#include <GL/glew.h> 

// OpenGL library 
#include <GL/gl.h>

// OpenGL Utility library
#include <GL/glu.h>

// OpenGL Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <QGLFormat>
#include <QGLWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QTimer>
#include <stack>

#include "camera.h"
#include "shader.h"
#include "grid.h"

class Viewer : public QGLWidget {
 public:
  Viewer(char *filename,const QGLFormat &format=QGLFormat::defaultFormat());
  ~Viewer();
  
 protected :
  virtual void paintGL();
  virtual void initializeGL();
  virtual void resizeGL(int width,int height);
  virtual void keyPressEvent(QKeyEvent *ke);
  virtual void mousePressEvent(QMouseEvent *me);
  virtual void mouseMoveEvent(QMouseEvent *me);

 private:
  // OpenGL objects creation
  void createVAO();
  void deleteVAO();

  void createFBO();
  void initFBO();
  void deleteFBO();

  void sendTexture(GLuint texID, QImage texture);
  void createTextures();
  void deleteTextures();

  void createShaders();
  void enableShader(unsigned int shader=0);
  void disableShader();
  void deleteShaders();
  void reloadShaders();
  
  // drawing functions 
  void drawVao();
  void drawQuad();

  QTimer        *_timer;    // timer that controls the animation

  Grid   *_grid;   // the grid
  Camera *_cam;    // the camera

  GLuint _vaoQuad;
  GLuint _quad;

  glm::vec3 _light;  // light direction
  glm::vec3 _motion; // motion offset for the noise texture 
  bool      _mode;   // camera motion or light motion
  unsigned int   _currentshader; // current shader index
  Shader *_shaderSecondPass; // shader used to compute lighting
  float _time;

  std::vector<std::string> _vertexFilenames;   // all vertex filenames
  std::vector<std::string> _fragmentFilenames; // all fragment filenames
  std::vector<Shader *>    _shaders;           // all the shaders 

  // vbo/vao ids 
  GLuint _vaoTerrain;
  GLuint _terrain[2];

  // texture ids 
  GLuint _lavaTexIds[7];
  GLuint _stoneTexIds[5];
  GLuint _distortionMapId;

  // fbo id
  GLuint _fbo;

  // render texture ids 
  GLuint _rendNormalId;
  GLuint _rendColorId;
  GLuint _rendDepthId;

  unsigned int _ndResol;
};

#endif // VIEWER_H
